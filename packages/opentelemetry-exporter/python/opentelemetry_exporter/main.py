# -*- mode: python; python-indent: 4 -*-
import logging
import select
import socket

import ncs
from _ncs import events
import _ncs

from bgworker import background_process

from opentelemetry import metrics, trace
from opentelemetry.exporter import jaeger
from opentelemetry.exporter import prometheus
from opentelemetry.sdk.metrics import Counter, MeterProvider
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor
import prometheus_client

from . import ptrace

def OpenTelemetryExporter():
    """Function for running as NSO background worker to subscribe to NSO
    progress-trace notification and export to tracing system
    """
    log = logging.getLogger('opentelemetry-exporter')
    log.info('OpenTelemetry progress-trace exporter started')

    # Read in configuration from CDB
    with ncs.maapi.single_read_trans('opentelemetry-exporter', 'system') as t:
        root = ncs.maagic.get_root(t)
        ot_config = root.progress.opentelemetry
        cfg_reporting_host = ot_config.reporting_host
        cfg_reporting_port = ot_config.reporting_port
        cfg_logging = ot_config.logging
        # turn YANG list of name/value into python dict
        cfg_extra_tags = dict(map(lambda x: [x.name, x.value], ot_config.extra_tags))

    log.debug(f"extra-tags: {cfg_extra_tags}")

    jaeger_exporter = jaeger.JaegerSpanExporter(
        service_name="NSO",
        agent_host_name=cfg_reporting_host,
        agent_port=cfg_reporting_port,
    )

    trace.set_tracer_provider(TracerProvider())
    trace.get_tracer_provider().add_span_processor(
        SimpleExportSpanProcessor(jaeger_exporter)
    )

    tracer = trace.get_tracer('opentelemetry-exporter')

    prometheus_client.start_http_server(port=8000, addr="0.0.0.0")
    metrics.set_meter_provider(MeterProvider())
    meter = metrics.get_meter(__name__)
    exporter = prometheus.PrometheusMetricsExporter("nso")
    metrics.get_meter_provider().start_pipeline(meter, exporter, 5)

    ptrace.process_traces(meter, tracer, get_ptrace_notifs(), extra_tags=cfg_extra_tags)


def get_ptrace_notifs():
    """Generator that yields NSO progress-trace events, coming from the NSO
    notification API that we subscribed to
    """
    # set up listener for NSO notifications
    event_sock = socket.socket()
    mask = events.NOTIF_PROGRESS
    noexists = _ncs.Value(init=1, type=_ncs.C_NOEXISTS)
    notif_data = _ncs.events.NotificationsData(heartbeat_interval=1000, health_check_interval=1000, stream_name='whatever', start_time=noexists, stop_time=noexists, verbosity=ncs.VERBOSITY_VERY_VERBOSE)
    events.notifications_connect2(event_sock, mask, ip='127.0.0.1', port=ncs.NCS_PORT, data=notif_data)

    while True:
        (readables, _, _) = select.select([event_sock], [], [])
        for readable in readables:
            if readable == event_sock:
                event_dict = events.read_notification(event_sock)
                pev = event_dict['progress']

                # TODO: get nice name from NSO?
                type_map = { 1: 'start', 2: 'stop', 3: 'info' }
                pev['type'] = type_map[pev['type']]

                yield pev


class Main(ncs.application.Application):
    worker = None

    def setup(self):
        self.worker = background_process.Process(self, OpenTelemetryExporter, config_path='/progress:progress/opentelemetry-exporter:opentelemetry/enabled')
        self.worker.start()

    def teardown(self):
        self.worker.stop()
