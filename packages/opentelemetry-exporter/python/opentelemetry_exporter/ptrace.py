"""NSO progress-trace related stuffz
"""
import csv
import datetime
import logging
import re
import sys

from opentelemetry import trace
from opentelemetry.exporter import jaeger
from opentelemetry.sdk.metrics import Counter
from opentelemetry.sdk.trace import TracerProvider
from opentelemetry.sdk.trace.export import SimpleExportSpanProcessor

def process_traces(meter, tracer, stream, extra_tags=None):
    """Process a stream of progress-trace events and export to tracing system

    This expects a generator as input that will keep yielding progress-trace
    events.
    """
    log = logging.getLogger(__name__)

    # who is currently holding the transaction-lock?
    # the stop event of the "grabbing transaction lock" span is the start of
    # holding the transaciton-lock and the info message "releasing the
    # transaction-lock" marks the end of it
    tlock_holder = None

    # this contains depth and spans keyed per tid
    tids = {}

    if extra_tags is None:
        extra_tags = {}

    counters = {
        'progress_trace_events': meter.create_metric(
            name="progress_trace_events",
            description="Number of progress trace events",
            unit="1",
            value_type=int,
            metric_type=Counter,
        )
    }

    for pev in stream:
        counters['progress_trace_events'].add(1, {'type': pev['type']})
        untag = ('type')
        # create copy of pev as kvs but exclude empty string items and certain
        # keys which we don't want (stored in untag)
        kvs = {k: v for k, v in pev.items() if k not in untag or v == ''}

        kvs.update(extra_tags)

        # create struct for our transaction, keyed by tid
        if pev['tid'] not in tids:
            root_kvs = {k: v for k, v in kvs.items() if k not in ('msg')}
            log.debug(f"{str(pev['tid']):10s}{pev.get('device', ''):10s}starting transaction root span")
            root_span = tracer.start_span('transaction',
                                          attributes=root_kvs,
                                          start_time=pev['timestamp']*1000)
            tids[pev['tid']] = {
                'dimensions': {},
                'spans': [],
                'locks': {},
                'root': root_span
            }
        our_tid = tids[pev['tid']]
        spans = our_tid['spans']
        locks = our_tid['locks']

        if 'device' in pev and pev['device'] != "":
            device_span = True
        else:
            device_span = False

        # determine dimensional key
        dim_key = device_span and pev['device'] or ''

        if dim_key not in our_tid['dimensions']:
            # first encounter of this dimension
            our_tid['dimensions'][dim_key] = { 'depth': 0, 'last_by_depth': {}, 'device_trace': device_span }

        our_dim = our_tid['dimensions'][dim_key]
        depth = our_dim['depth']
        device_trace = our_dim['device_trace']

        # figure out parent span
        parent_span = None
        if depth == 0:
            parent_span = our_tid['root']
            if device_span and not device_trace and '' in our_tid['dimensions']:
                parent_depth = our_tid['dimensions']['']['depth']-1
                parent_span = our_tid['dimensions']['']['last_by_depth'][parent_depth]
        else:
            parent_span = our_dim['last_by_depth'][depth-1]

        if pev['type'] == 'start': # start of a span
            log.debug(f"{str(pev['tid']):10s}{pev.get('device', ''):10s}{depth}>>{'  '*depth} {pev['msg']}")
            span = tracer.start_span(pev['msg'],
                                     parent=parent_span,
                                     attributes=kvs,
                                     start_time=pev['timestamp']*1000)
            our_dim['last_by_depth'][depth] = span
            # for the start of the "grabbing transaction lock" span, if
            # we know the transaction-lock holder, add it as an event
            if pev['msg'] == 'grabbing transaction lock' and tlock_holder is not None:
                span.add_event('trans-lock holder changed', { 'transaction-lock-holder': tlock_holder }, pev['timestamp'] * 1000)

            spans.append(span)
            depth += 1

        elif pev['type'] == 'stop': # end of a span
            log.debug(f"{str(pev['tid']):10s}{pev.get('device', ''):10s}{depth}<<{'  '*(depth-1)} {pev['msg']}")

            # maintain transaction-lock holder state
            # the stop message "grabbing transaction lock" marks the start of
            # holding the transaction-lock
            if pev['msg'] == 'grabbing transaction lock':
                tlock_holder = pev['tid']
                # we use '' to mean the trans-lock
                locks[''] = True
                tlock_span = tracer.start_span('holding transaction lock',
                                               parent=parent_span,
                                               attributes=kvs,
                                               start_time=pev['timestamp'] * 1000)
                our_tid['tlock_span'] = tlock_span
                # we just got the transaction-lock - find other
                # transactions waiting for the lock and add a KV-log
                # entry so it shows they are waiting for us
                # NOTE: this is a relatively expensive search, looping
                # through all current state
                for otid, oval in tids.items():
                    if otid == pev['tid']:
                        continue
                    for ospan in oval['spans']:
                        if ospan.end_time is None and ospan.name == 'grabbing transaction lock':
                            ospan.add_event('trans-lock-holder changed',
                                            {'transaction-lock-holder': tlock_holder},
                                            pev['timestamp'] * 1000)

            if pev['msg'] == 'taking device lock':
                locks[pev['device']] = True

            depth -= 1
            if depth < 0:
                log.warning("Stack depth should never be < 0. Likely startup during ongoing transaction - ignoring event & resetting stack.")
                # This could also be caused by unbalanced start & stop event
                # but then we'll likely catch that in our message comparison
                # below. Unbalanced start & stop is either a processing bug (in
                # this script) or if NSO truly is emitting unbalanced event it
                # is a probably a bug in NSO.
                depth = 0
                spans = {}
            else:
                # find matching start entry, it is the last entry with the same depth as us
                match = our_dim['last_by_depth'][depth]

                # check that the message matches up
                if (match.name != pev['msg'] or ('device' in match.attributes and match.attributes['device'] != dim_key)) and not (re.match('entering', match.name) and re.match('leaving', pev['msg'])):
                    log.warning(f"Message mismatch with depth-matched event, current message: {pev['msg']}  matched message: {match.name}")

                # close the span
                match.end(pev['timestamp']*1000)


        elif pev['type'] == 'info':
            log.debug(f"{str(pev['tid']):10s}{pev.get('device', ''):10s}{depth}--{'  '*(depth-1)} {pev['msg']}")
            # maintain transaction-lock holder state
            # the info message "releasing transaction lock" marks the end
            if pev['msg'] == 'releasing transaction lock':
                del locks['']
                tlock_holder = None
                if 'tlock_span' not in our_tid:
                    log.warning(f"WARNING: trying to close trans-lock span, but none open - tid: {pev['tid']}")
                else:
                    our_tid['tlock_span'].end(pev['timestamp'] * 1000)

            if pev['msg'] == 'releasing device lock':
                del locks[pev['device']]

            for span in reversed(spans):
                # match up with the last event that is on the same device as this event
                if dim_key != '':
                    if 'device' in span.attributes and span.attributes['device'] != dim_key:
                        continue
                if span.end_time is None:
                    span.add_event(pev['msg'], kvs, pev['timestamp'] * 1000)
                    break
        else:
            raise ValueError(f"Unhandled progress-trace event type {pev['type']}")

        # finish trace
        # what's the exit condition?
        # There is no root span from NSO, so we could see multiple depth=0
        # spans within the same transaction - how do we know when we are done?
        # Reasonably we must have released all locks, so if we don't have any
        # locks held and then get to depth=0 we are *probably*? done
        # if we are at the end of the root span, clean up state
        if depth == 0 and len(locks) == 0:
                log.debug(f"{str(pev['tid']):10s}{pev.get('device', ''):10s}ending transaction root span")
                our_tid['root'].end(pev['timestamp']*1000)
                del tids[pev['tid']]
        else: # otherwise preserve our state
            our_dim['depth'] = depth
            our_tid['spans'] = spans
            our_tid['locks'] = locks

        # something is wrong if this ballons in size, 1000 is a very high limit
        # given how NSO normally operates, so this *must* be a bug somewhere.
        # Most likely unbalanced start / stop messages.
        assert len(tids) < 1000



def csv_ptrace_reader(filename):
    """Generator that yields progress-trace events as read from a
    progress-trace CSV file

    This will try to workaround deficiencies in older CSV exports where the
    type field isn't present. We try to recreate it based on the messages but
    this is somewhat error prone, at least theoretically, so don't be surprised
    if this breaks.
    """
    # This is a translating of CSV field names to the expected keys of a
    # progress-trace event as emitted by the NSO notification API
    xlate = {
        'EVENT TYPE': 'type',
        'TIMESTAMP': 'timestamp',
        'DURATION': 'duration',
        'SESSION ID': 'usid',
        'TID': 'tid',
        'TRANSACTION ID': 'tid',
        'DATASTORE': 'datastore_name',
        'CONTEXT': 'context',
        'SUBSYSTEM': 'subsystem',
        'PHASE': 'phase',
        'SERVICE': 'service',
        'SERVICE PHASE': 'service_phase',
        'COMMIT QUEUE ID': 'cqid',
        'NODE': 'node',
        'DEVICE': 'device',
        'DEVICE PHASE': 'device_phase',
        'PACKAGE': 'package',
        'MESSAGE': 'msg',
        'ANNOTATION': 'annotation'
    }

    # These are the start (1) and stop (2) messages for spans that have
    # unbalanced start and stop messages, i.e. where they are not the same.
    span_unbalanced = {
        'entering abort phase': 'start',
        'entering commit phase': 'start',
        'entering prepare phase': 'start',
        'entering validate phase': 'start',
        'entering write-start phase': 'start',
        'leaving abort phase': 'stop',
        'leaving commit phase': 'stop',
        'leaving prepare phase': 'stop',
        'leaving validate phase': 'stop',
        'leaving write-start phase': 'stop'
    }

    # Names of spans that have balanced start and stop messages
    span_names = {
        'applying FASTMAP reverse diff-set',
        'applying transaction',
        'check configuration policies',
        'check data kickers',
        'create',
        'creating rollback file',
        'grabbing transaction lock',
        'mark inactive',
        'post-modification',
        'pre validate',
        'pre-modification',
        'run dependency-triggered validation',
        'run pre-transform validation',
        'run service',
        'run transforms and transaction hooks',
        'run validation over the changeset',
        'saving FASTMAP reverse diff-set and applying changes',
    }

    # Known info messages. Anything not in here and not matching a list of
    # regexps will result in a warning - possibly something we want to look
    # into.
    known_info = {
        'all commit subscription notifications acknowledged',
        'commit',
        'conflict deleting zombie, adding re-deploy to sequential side effect queue',
        'nano service deleted',
        'prepare',
        're-deploy merged in queue',
        're-deploy queued',
        'received commit from all (available) slaves',
        'received prepare from all (available) slaves',
        'releasing device lock',
        'releasing transaction lock',
        'send NED close',
        'send NED commit',
        'send NED connect',
        'send NED get-trans-id',
        'send NED initialize',
        'send NED is-alive',
        'send NED noconnect',
        'send NED persist',
        'send NED prepare',
        'send NED prepare-dry',
        'send NED reconnect',
        'send NED show',
        'sending confirmed commit',
        'sending confirming commit',
        'sending edit-config',
        'transaction empty',
        'write-start',
        'zombie deleted',
        'send NED show',
        'send NED get-trans-id',
        'releasing device lock',
        're-deploy queued',
        're-deploy merged in queue',
    }

    known_info_re = {
        'transaction lock queue length:',
        'SNMP connect to',
        '(SNMP|SSH) connecting to',
        'reuse SSH connection',
        'SNMP USM engine id',
        'evaluated behaviour tree pre-condition',
        'component {',
        'delivering commit subscription notifications',
    }

    with open(filename) as csvfile:
        start_seen = {}

        csvreader = csv.DictReader(csvfile)
        for raw in csvreader:
            row = {}
            for key, val in raw.items():
                if key == 'TIMESTAMP':
                    # convert timestamp
                    ts = datetime.datetime.strptime(val, '%Y-%m-%dT%H:%M:%S.%f')
                    val = int(ts.strftime("%s%f"))
                row[xlate[key]] = val

            durm = re.search(r'(.*) \[([0-9]+) ms\]$', row['msg'])
            if durm:
                if row['duration'] != '':
                    print("ERROR: got duration in msg but duration set")
                row['msg'] = durm.group(1)
                row['duration'] = durm.group(2)

            if 'type' not in row:
                if row['msg'].endswith('...'):
                    row['type'] = 'start'
                    row['msg'] = row['msg'].replace('...', '')
                    start_seen[row['msg']] = True

                elif re.search(' (done|error|ok)$', row['msg']):
                    row['type'] = 'stop'
                    row['msg'] = re.sub(' (done|error|ok)$', '', row['msg'])
                    if row['msg'] in start_seen:
                        del start_seen[row['msg']]

                elif row['msg'] in span_unbalanced:
                    row['type'] = span_unbalanced[row['msg']]

                elif row['msg'] in span_names:
                    if row['msg'] in start_seen:
                        row['type'] = 'stop'
                        del start_seen[row['msg']]
                    else:
                        row['type'] = 'start'
                        start_seen[row['msg']] = True
                else:
                    row['type'] = 'info'
                    if [ i for i in known_info_re if re.match(i, row['msg'])]:
                        pass
                    elif (re.match('evaluated behaviour tree', row['msg'])
                          or re.match('component.*state', row['msg'])
                         ):
                        pass
                    elif row['msg'] not in known_info:
                        print(f"WARNING: unknown message, assuming info: {row['msg']}")

            yield row


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--debug', action='store_true')
    parser.add_argument('--jaeger-host', default='localhost')
    parser.add_argument('--jaeger-port', type=int, default=6831)
    parser.add_argument('--csv')
    parser.add_argument('--extra-tags', action="append", default=[])
    args = parser.parse_args()

    extra_tags = {}
    for et in args.extra_tags:
        extra_tags[et.split("=")[0]] = et.split("=")[1]

    if args.debug:
        logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

    if args.csv:
        jaeger_exporter = jaeger.JaegerSpanExporter(
            service_name="NSO",
            agent_host_name=args.jaeger_host,
            agent_port=args.jaeger_port,
        )

        trace.set_tracer_provider(TracerProvider())
        trace.get_tracer_provider().add_span_processor(
            SimpleExportSpanProcessor(jaeger_exporter)
        )

        tracer = trace.get_tracer('opentelemetry-exporter')

        process_traces(tracer, csv_ptrace_reader(args.csv), extra_tags)
